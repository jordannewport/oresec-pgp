\documentclass{lug}

\usepackage{etoolbox}
\usepackage{textcomp}
\usepackage[nodisplayskipstretch]{setspace}
\usepackage{xspace}
\usepackage{verbatim}
\usepackage{multicol}
\usepackage{soul}
\usepackage{svg}

\usepackage{amsmath,amssymb,amsthm}

\usepackage[linesnumbered,commentsnumbered,ruled,vlined]{algorithm2e}
\newcommand\mycommfont[1]{\footnotesize\ttfamily\textcolor{blue}{#1}}
\SetCommentSty{mycommfont}
\SetKwComment{tcc}{ \# }{}
\SetKwComment{tcp}{ \# }{}

\usepackage{siunitx}

\usepackage{tikz}
\usepackage{pgfplots}
\usetikzlibrary{decorations.pathreplacing,calc,arrows.meta,shapes,graphs}

\AtBeginEnvironment{minted}{\singlespacing\fontsize{10}{10}\selectfont}
\usefonttheme{serif}

\makeatletter
\patchcmd{\beamer@sectionintoc}{\vskip1.5em}{\vskip0.5em}{}{}
\makeatother

% Math stuffs
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\lcm}{\text{lcm}}
\newcommand{\Inn}{\text{Inn}}
\newcommand{\Aut}{\text{Aut}}
\newcommand{\Ker}{\text{Ker}\ }
\newcommand{\la}{\langle}
\newcommand{\ra}{\rangle}

\newcommand{\yournewcommand}[2]{Something #1, and #2}
\newcommand{\zshrc}{\texttt{\textasciitilde/.zshrc}}

\newenvironment{question}[1]{\par\textbf{Question #1.}\par}{}

\newcommand{\pmidg}[1]{\parbox{\widthof{#1}}{#1}}
\newcommand{\splitslide}[4]{
    \noindent
    \begin{minipage}{#1 \textwidth - #2 }
        #3
    \end{minipage}%
    \hspace{ \dimexpr #2 * 2 \relax }%
    \begin{minipage}{\textwidth - #1 \textwidth - #2 }
        #4
    \end{minipage}
}

\newcommand{\frameoutput}[1]{\frame{\colorbox{white}{#1}}}

\newcommand{\tikzmark}[1]{%
\tikz[baseline=-0.55ex,overlay,remember picture] \node[inner sep=0pt,] (#1)
{\vphantom{T}};
}

\newcommand{\braced}[3]{%
    \begin{tikzpicture}[overlay,remember picture]
        \draw [thick,decorate,decoration={brace,raise=1ex,amplitude=4pt},blue] (#2.south west-|T1.south west) -- node[anchor=west,left,xshift=-1.8ex,text=olive]{#3} (#1.north west-|T1.south west);
    \end{tikzpicture}
}

\title{PGP}
\author{Sumner Evans and David Florness}
\institute{Mines Cybersecurity Club}

\begin{document}

\begin{frame}{Overview}
    \begin{itemize}
        \item \textbf{Theory}
            \begin{itemize}
                \item Introduction to cryptography
                \item Symmetric vs. Asymmetric encryption
                \item PGP and GPG
                \item Why you should use it
            \end{itemize}

        \item \textbf{Usage}
            \begin{itemize}
                \item Making your own PGP Key
                \item Generating Revocation certificate
                \item PGP.mit.edu
                \item Signing other's keys
            \end{itemize}
    \end{itemize}
\end{frame}

\section{Theory}

\begin{frame}{What is cryptography?}

    Cryptography is the study of methods for securing communication and keeping
    data private in the presence of malicious actors called
    \textit{adversaries} or \textit{attackers}.\only<4>{\footnotemark}
    \pause

    Historically, C{\ae}sar Ciphers and later more complicated technologies such
    as the Enigma machine were used to secure communications.
    \pause

    However, due to the advent of modern computing we can \textit{brute force}
    these methods, and we need methods which are resistant to the amounts of
    computing power at our disposal.
    \pause

    \vspace{3em}

    \tiny
    \footnotetext*[1]{
        We will cover the very basics of cryptography here, with a focus on how
        PGP.  However, there's a lot more in the field, and CSCI 473
        Cryptography is a good class to take if you are interested in a deeper
        understanding of cryptography.
    }

\end{frame}

\begin{frame}{Terminology}
    \begin{description}
        \item[Plaintext] Also known as the \textit{message}. The text which we
            are trying to keep secret.
        \item[Ciphertext] The unintelligible form of the message.
        \item[Encrypt] The act of turning plaintext into ciphertext.
        \item[Decrypt] The act of turning ciphertext into plaintext.
        \item[Cryptographic Algorithm] An algorithm for encrypting or decrypting
            (often both) ciphertext or plaintext.
        \item[Key] A \textit{secret} used in the encryption or decryption
            process.
    \end{description}
\end{frame}

\begin{frame}{Symmetric key cryptography I}

    \textit{Symmetric-key algorithms} are cryptographic algorithms which use the
    same cryptographic key for both encrypting and decrypting the
    data.\footnote[frame]{Sometimes, the encryption and decryption keys are
        identical, and other times they are related to one another via a simple
        transformation.}
    \pause

    \textbf{Example:} C{\ae}sar cipher. The key tells you how many letters to
    shift up or down in the alphabet, depending on whether you are encrypting or
    decrypting the message.
    \pause

    \textbf{Common modern symmetric key algorithms:}
    \begin{itemize}
        \item DES (1977, still used for many things today)
        \item AES (2001, one of the most prevalent)
    \end{itemize}
\end{frame}

\begin{frame}{Symmetric key cryptography II}
    \textbf{Advantages:}
    \begin{itemize}
        \item \textbf{Extremely secure.} Currently, we think they will be
            resistant to quantum computers.
        \item \textbf{Relatively fast.} Fancy mathematics required to perform
            the encryption and decryption is minimal.
    \end{itemize}
    \pause

    \textbf{Disadvantages:}
    \begin{itemize}
        \item \textbf{Sharing the key.} Need a complicated algorithm such as
            Diffie-Hellman to share the key securely.
    \end{itemize}
    \pause

    \textbf{Used by:}
    \begin{itemize}
        \item \textbf{TLS and SSL}, the technology behind HTTPS.
    \end{itemize}

    \vspace{2em}
    \tiny
    See
    \url{https://itstillworks.com/advantages-disadvantages-symmetric-key-encryption-2609.html}
\end{frame}

\begin{frame}{Asymmetric cryptography I}

    \textit{Asymmetric-key algorithms}, also known as \textit{public-key
        algorithms} are cryptographic systems which use pairs of keys: a
    \textbf{public key} which ``can be spread like
    butter''\footnote[frame]{Keith Hellman}, and a \textbf{private key} which
    should be kept as secure as possible.
    \pause

    The public key is used to encrypt messages, and the private key is used to
    decrypt ciphertext. The private key is also used to create \textit{digital
        signatures}.

    \textbf{Common modern asymmetric key algorithms:}
    \begin{itemize}
        \item RSA (1977, still used for many things today, uses factorization of
            primes to achieve security)
    \end{itemize}

\end{frame}

\begin{frame}{Asymmetric cryptography II}

    \textbf{Advantages:}
    \begin{itemize}
        \item \textbf{Sharing the key.} It is super easy to share the key (no
            need for a secure tunnel for sharing.)
        \item \textbf{Digital signing.} You can cryptographically ``sign'' a
            message and the signature can be verified to assert authenticity.
    \end{itemize}
    \pause

    \textbf{Disadvantages:}
    \begin{itemize}
        \item \textbf{Relatively slow.} Fancy math is require to make it work.
        \item \textbf{Vulnerable to quantum computers.} Due to Shor's algorithm
            which can factor primes in $O(\log n)$ time.
    \end{itemize}
    \pause

    \textbf{Used by:}
    \begin{itemize}
        \item \textbf{Diffie-Hellman}, the key distribution algorithm.
    \end{itemize}

\end{frame}

\begin{frame}{PGP}

    Pretty Good Privacy (PGP) is a data encryption, decryption, and signing
    program that follows the \textbf{OpenPGP} standard.

    PGP provides all of the advantages of asymmetric cryptography in an actual
    program which you can install and use.
    \pause

    Some common uses of PGP include signing, encrypting, and decrypting emails,
    files, folders, and even whole disk partitions.

    PGP provides an easy way to specify a recipient (or multiple recipients) to
    encrypt a message to. All you need is their public key(s).

\end{frame}

\begin{frame}{How encryption and decryption works}
    \begin{center}
        \includegraphics[width=0.74\textwidth]{./graphics/PGP_diagram}
    \end{center}
\end{frame}

\begin{frame}{How signatures work}
    \textbf{Creating a signature}
    \begin{enumerate}
        \item Compute a hash (also called a message digest) from the plaintext.
        \item Create a digital signature by using the sender's private key to
            encrypt the hash.
    \end{enumerate}

    \textbf{Verifying a signature}
    \begin{enumerate}
        \item Compute the digest (hash) of the plaintext ourselves.
        \item Decrypt the digital signature using the sender's public key.
        \item Compare the hashes and verify that they are the same.
    \end{enumerate}

    \vspace{1em}

    \tiny
    See
    \url{https://www.comparitech.com/blog/information-security/pgp-encryption/}
\end{frame}

\begin{frame}{GPG}
    \begin{center}
        \includegraphics[width=0.5\textwidth]{./graphics/Gnupg_logo}
    \end{center}

    GNU Privacy Guard (GnuPG or GPG), a free-software replacement for Symantec's
    PGP cryptographic software suite.
    \pause

    Typically, you interface with it using the \texttt{gpg} CLI tool, but there
    are GUIs available. Most Linux distributions already have it installed
    because package managers often use \texttt{gpg} to verify packages.
\end{frame}

\begin{frame}{What is GPG good for?}
    There are many use cases for GPG including\dots

    \begin{description}[<+->]
        \item[Email] Signing and encrypting email.\footnote[frame]{For more
                information, come to LUG this Thursday at 18:00 for a talk about
                how insecure email is and how to remedy it with good email
                clients which support GPG.}
        \item[Encrypted Backups] Tools such as Duplicity support GPG encrypted
            backups.
        \item[Package Verification] Most distribution package managers use GPG
            signatures to verify package integrity (to prevent man-in-the-middle
            attacks).
        \item[Password Managers] The \texttt{pass} password
            manager\footnote[frame]{\url{https://www.passwordstore.org/}} uses
            GPG files to securely store your passwords.
    \end{description}

\end{frame}

\section{Usage}

\begin{frame}[fragile,allowframebreaks]{Making your own PGP key}
  \begin{enumerate}
  \item To generate a key:
    \begin{minted}{bash}
      gpg --full-gen-key
    \end{minted}
  \item This will ask a series of questions, including what your name and email
    are.
  \item This is the name and email that other PGP users see when they use your
    key.
  \item You can attach multiple email addresses to a single key:
    \begin{minted}{bash}
      gpg --edit-key 1983C33A3F685A5DF524D7A5D61EB041DA38F0FA
      gpg> adduid
      Real name: Jim Morrison
      Email address: jim@example.com
    \end{minted}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]{Generating Revocation certificate}
  \begin{enumerate}
  \item If you lose and/or fogret the password to your key, you're completely
    screwed
  \item UNLESS you generate a revocation certificate ahead of time:
    \begin{minted}{bash}
      gpg --output revoke.asc --gen-revoke 1983C33A3F685A5DF524D7A5D61EB041DA38F0FA
    \end{minted}
  \item In newer versions of GPG, this is done for you and the certificate is
    put in \texttt{\$HOME/.gnupg/openpgp-revocs.d}, but double-check just to be
    sure.
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]{Keyservers}
  \begin{enumerate}
  \item To share your keys with others:
    \begin{minted}{bash}
      gpg --send-keys 1983C33A3F685A5DF524D7A5D61EB041DA38F0FA
    \end{minted}
  \item This should automatically find a keyserver; if not:
    \begin{minted}{bash}
      gpg --keyserver pgp.mit.edu --send-keys 1983C33A3F685A5DF524D7A5D61EB041DA38F0FA
    \end{minted}
  \item Then, to download keys:
    \begin{minted}{bash}
      gpg --recv-keys C0793CEA7F4107C3FE884F86060576B7352B8DE3
    \end{minted}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]{Keybase}
  \begin{enumerate}
  \item Up until now, we have interacted with PGP only via a shell. This is
    probably why very few people actually use PGP.\@
  \item Keybase.io is trying to change this with a friendly web interface. There
    are also Android and iOS apps.
  \item Features include following other users, proving you own a Reddit,
    Twitter, GitHub (etc.) account, and sending encrypted messages and files
    easily to other users.
  \item A CLI interface also exists, if that's your thing. Using this you can
    also upload any existing PGP keys that you have on your machine.
  \end{enumerate}
  \begin{center}
    \includesvg[width=0.2\textwidth]{graphics/Keybase_logo.svg}
  \end{center}
\end{frame}

\begin{frame}{Man-in-the-middle attacks}
  How can you be sure that your downloaded keys are authentic? Scumbags can
  still intercept key download requests and provide fake keys that they know the
  private part to.

  \includesvg[width=\textwidth]{graphics/Man_in_the_middle_attack.svg}
\end{frame}

\begin{frame}[fragile,allowframebreaks]{Signing others' keys and the Web of Trust}
  The solution is to meet up with the key owners and check their
  fingerprints:
\begin{verbatim}
$ gpg --list-key --fingerprint "David Florness"

pub   rsa2048 2017-03-18 [SC] [expires: 2019-03-18]
      C079 3CEA 7F41 07C3 FE88  4F86 0605 76B7 352B 8DE3
\end{verbatim}

  Once you have verified that the fingerprints match, you can use your
  key to ``sign'' your friend's key so that anyone else can verify your friend
  if he/she has verified \textit{your} key.
  \begin{minted}{bash}
    gpg --sign-key "Ray Manzarek"
  \end{minted}
  This feature embodies the Web of Trust, since over time you will accumulate a
  slew of keys that you have verified and you trust that each trusted key will
  sign other keys legitimately.
\end{frame}

\begin{frame}{References}
    \small
    \begin{itemize}
        \item \url{https://en.wikipedia.org/wiki/Cryptography}
        \item \url{https://en.wikipedia.org/wiki/Symmetric-key\_algorithm}
        \item \url{https://en.wikipedia.org/wiki/Pretty\_Good\_Privacy}
    \end{itemize}
\end{frame}

\end{document}
% Local Variables:
% TeX-command-extra-options: "-shell-escape"
% End:
