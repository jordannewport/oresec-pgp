#
# Makefile
#

MAINPDFFILE=pgp.pdf
EXAMPLEFILES=$(wildcard examples/*.tex)
EXAMPLEPDFFILES=$(patsubst %.tex,%.pdf,$(EXAMPLEFILES))
LATEX_COMPILER=xelatex -shell-escape

all: $(MAINPDFFILE)

examples/%.pdf: examples/%.tex
	$(LATEX_COMPILER) -output-directory=examples $<

pgp.pdf: pgp.tex $(EXAMPLEPDFFILES) lug.cls
	$(LATEX_COMPILER) $<

clean:
	rm -f *.log *.aux *.log *.out *.bbl *.blg *.vrb *.snm *.toc *.pdf *.nav
	rm -rf _minted-pgp

.PHONY: all clean
