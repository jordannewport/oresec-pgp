OreSec PGP Presentation
=======================

:Author: Sumner Evans and David Florness

Topics Covered
--------------

- Theory

  - What is cryptography?
  - What is public-private key cryptography?
  - What is PGP?
  - What is GPG?
  - Web of Trust
  - What is it good for?

- Usage

  - Making your own PGP Key
  - Generating Revocation certificate
  - PGP.mit.edu
  - Signing other's keys


Additional Resources
--------------------

- Jack's Presentation: https://github.com/jackrosenthal/lug-pgp-presentation
